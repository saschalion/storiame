var LikeButton = React.createClass({
    getInitialState: function() {
        var item = this.props.item;

        return {
            liked: item.objectPreview.context.liked,
            likes: item.objectPreview.stats.likes
        };
    },

    handleClick: function() {
        this.setState({
            liked: !this.state.liked,
            likes: this.state.liked ? this.state.likes - 1 : this.state.likes + 1
        });

        this.likeInit();
    },

    likeInit: function() {
        var item = this.props.item;
        var storyId = item.objectPreview.storyId;
        var momentId = item.objectPreview.id;
        var type = this.state.liked ? 'delete' : 'post';

        var like = function() {
            $.ajax({
                url: 'https://storia.me/api/core/stories/' + storyId + '/moments/' + momentId + '/like',
                xhrFields: {
                    withCredentials: true
                },
                type: type
            });
        };

        if ( window.onLine ) {
            like();
        }

        window.onLineHandler = function() {
            like();
        };
    },

    render: function() {
        if ( this.state.liked ) {
            return (
                <a className="b-moment__actions-link _state_active" onClick={ this.handleClick }>
                    <i className="b-moment__actions-icon fa fa-heart"></i>
                    <i className="b-moment__actions-text">
                        { this.state.likes }
                    </i>
                </a>
            );
        } else {
            return (
                <a className="b-moment__actions-link" onClick={ this.handleClick }>
                    <i className="b-moment__actions-icon fa fa-heart-o"></i>
                    <i className="b-moment__actions-text">
                        { this.state.likes }
                    </i>
                </a>
            );
        }
    }
});

var Moment = React.createClass({
    getAttachment: function(data) {
        var attachment = '';
        var title;
        var isNumeric = function(title) {
            return /[0-9]/.test(title);
        };

        if ( Object.keys(data).length ) {
            var length = Object.keys(data).length;
            var lastIndex = length == 1 ? 0 : length - 1;

            for(var i = lastIndex; i > 0; i--) {
                var file = data[i].file;

                if ( !isNumeric(file.title) ) {
                    attachment = file.path;
                    title = file.title;
                }
            }

            if ( !attachment ) {
                attachment = data[0].file.path;
                title = data[0].file.title;
            }
        }

        if ( attachment ) {
            return (
                <p className="b-moments__image-inner">
                    <img className="b-moments__image" src={attachment} alt={title} title={title}/>
                </p>
            );
        }
    },

    render: function() {
        var item = this.props.item;

        return (
            <article className="b-moments__item">
                <div className="b-moments__info clearfix">
                    <div className="b-moments__info-left">
                        <div className="b-moments__info-image-inner">
                            <img className="b-moments__avatar" src={item.objectPreview.owner.avatar.path} alt={item.objectPreview.owner.name}/>
                        </div>
                        <div className="b-moments__info-user-inner">
                            <div className="b-moments__info-user-name">
                                  { item.objectPreview.owner.name }
                            </div>
                        </div>
                    </div>
                    <div className="b-moments__info-right">
                        { item.objectPreview.storyTitle }
                    </div>
                </div>

                { item.objectPreview.title &&
                    <h2>
                        {item.objectPreview.title}
                    </h2>
                }

                { this.getAttachment(item.objectPreview.attachments) }

                <div className="b-moment__actions">
                    <LikeButton item={item} />
                </div>
            </article>
        );
    }
});

var Feed = React.createClass({
    getInitialState: function() {
        return {
            items: []
        };
    },

    componentDidMount: function() {
        var $this = this;

        var getMoments = function() {
            $.ajax({
                url: 'https://storia.me/api/feed/content',
                data: {
                    limit: 10
                },
                contentType: 'application/json',
                xhrFields: {
                    withCredentials: true
                }
            }).done(function(data) {
                $this.setState({
                    items: data.items
                });
            });
        };

        var json = {
            password: 'qwe123',
            token: '',
            remember: true
        };
        json = JSON.stringify(json);

        $.ajax({
            url: 'https://storia.me/api/acl/auth/Selfish/test_task@example.com?',
            type: 'post',
            dataType: 'json',
            data: json,
            contentType: 'application/json',
            xhrFields: {
                withCredentials: true
            }
        }).done(function() {
            getMoments();
        });
    },

    render: function() {
        return (
            <section>
                {this.state.items.map(function(item) {
                    return <Moment key={item.id} item={item} />;
                })}
            </section>
        );
    }
});

ReactDOM.render(<Feed />, document.getElementById('feed'));