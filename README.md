Test App for Storia.me
========================

* Open Terminal/Console
* Git clone https://github.com/saschalion/storiame
* cd storiame
* npm install
* bower update OR node_modules/bower/bin/bower update

* watch styles - npm run watch
* build styles - npm run build