var gulp = require('gulp');
var less = require('gulp-less');
var watch = require('gulp-watch');
var nib = require('nib');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');

var dirPath = 'styles';

gulp.task('css', function() {
    gulp.src([
            dirPath + '/main.less',
            dirPath + '/**/*.less']
    )
        .pipe(concat('main.styl'))
        .pipe(less({
            use: [nib()],
            compress: true
        }))
        .pipe(autoprefixer())
        .pipe(gulp.dest(dirPath));
});

gulp.task('watch', function() {
    watch(dirPath + '/**/*.less', function() {
        return gulp.src([
                dirPath + '/main.less',
                dirPath + '/**/*.less'
            ]
        )
            .pipe(sourcemaps.init({loadMaps: true}))
            .pipe(concat('main.less'))
            .pipe(less({
                use: [
                    nib()
                ]
            }))
            .pipe(autoprefixer({
                cascade: false
            }))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(dirPath));
    });
});

gulp.task('default', ['build']);
gulp.task('build', ['css']);
gulp.task('default', ['watch']);